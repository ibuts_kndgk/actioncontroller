//
//  ActionContainerViewController.swift
//  ActionContainerController
//
//  Created by Ihor Buts on 7/17/17.
//  Copyright © 2017 Ihor Buts. All rights reserved.
//

import UIKit

open class ActionContainerViewController: UIViewController {
    
    @IBOutlet fileprivate var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet private weak var buttonHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var wrapperView: UIView!
    @IBOutlet fileprivate var containerView: UIView!
    
    @IBOutlet open var titleLabel: UILabel!
    
    public var animationDuration: TimeInterval = 0.25
    public var containerViewController: UIViewController?
    
    public var containerViewHeight: CGFloat = 0 {
        didSet {
            containerHeightConstraint.constant = containerViewHeight
            self.view.layoutSubviews()
        }
    }
    
    public var buttonHeight: CGFloat? {
        didSet {
            
        }
    }
    
    public var attributedTitle: NSAttributedString?
    
    public init() {
        super.init(nibName:"ActionContainerViewController", bundle: Bundle(identifier: "KG.ActionContainerController"))
        
        transitioningDelegate = self
        modalPresentationStyle = .custom
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override open func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = attributedTitle
        assert(containerViewController != nil, "containerViewController can't be nil")
        guard let controller = containerViewController else { return }
        
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        
        containerHeightConstraint.constant = containerViewHeight 
        view.layoutSubviews()
        
        bottomLayoutConstraint.isActive = false
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        
        controller.didMove(toParentViewController: self)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        buttonHeightConstraint.constant = buttonHeight ?? 0
        
        view.layoutSubviews()
    }
    
    @IBAction private func didPress(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func cancelButtonDidPress(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension ActionContainerViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let view = touch.view else {
            return false
        }
        
        if let containerView = containerViewController?.view, view.isDescendant(of: containerView) || view.isDescendant(of: wrapperView){
            return false
        }
        return true
    }
}

extension ActionContainerViewController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension ActionContainerViewController: UIViewControllerAnimatedTransitioning {
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        let resultingColor: UIColor
        let animationOptions: UIViewAnimationOptions
        
        if bottomLayoutConstraint.isActive {
            bottomLayoutConstraint.isActive = false
            topLayoutConstraint.isActive = true
            
            resultingColor = UIColor.clear
            animationOptions = .curveEaseOut
            
        } else {
            let views: [String: Any] = ["view": view]
//            view.backgroundColor = UIColor.clear
            resultingColor = UIColor.lightGray.withAlphaComponent(0.5)
            animationOptions = .curveEaseIn
            containerView.addSubview(view)
            
            containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                                        options: [],
                                                                        metrics: nil,
                                                                        views: views))
            containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                        options: [],
                                                                        metrics: nil,
                                                                        views: views))
            containerView.setNeedsLayout()
            containerView.layoutIfNeeded()
            
            bottomLayoutConstraint.isActive = true
            topLayoutConstraint.isActive = false
        }
        
        UIView.animate(withDuration: animationDuration,
                       delay: 0.0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 1.0,
                       options: animationOptions,
                       animations:
            {
                containerView.layoutIfNeeded()
                self.view.backgroundColor = resultingColor
        }) { result in
            transitionContext.completeTransition(result)
        }
    }
}
