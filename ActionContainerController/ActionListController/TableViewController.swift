//
//  TableViewController.swift
//  ActionContainerController
//
//  Created by Ihor Buts on 7/17/17.
//  Copyright © 2017 Ihor Buts. All rights reserved.
//

import UIKit

public class Action {
    public typealias Completion = ((Action) -> Void)
    public var title: String?
    public var attributedTitle: NSAttributedString?
    public var completion: Completion?
    
    public init() {
        
    }
}

public class TableViewController: UITableViewController {
   
    fileprivate lazy var selectButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        button.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.cellHeight)
        button.addTarget(self, action: #selector(selectButtonDidPress(_:)), for: .touchDown)
        button.setTitleColor(UIColor(red: 19/255, green: 144/255, blue: 1, alpha: 1.0) , for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .disabled)
        button.setTitle("Select", for: .normal)
        button.backgroundColor = UIColor.groupTableViewBackground
        button.isEnabled = false
        
        return button
    }()
    
    var textAllignment: NSTextAlignment = .left
    var actionAttributes: [String : Any]?
    var cellHeight: CGFloat = 43.5
    var multySelection: (([Action]) -> Void)? {
        didSet {
            tableView.reloadData()
        }
    }
    
    var actions: [Action]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if multySelection != nil {
            tableView.allowsMultipleSelection = true
        }
        
        tableView.bounces = false
        tableView.showsVerticalScrollIndicator = false
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCellIdentifier")
    }
    
    fileprivate func updateSelectButton() {
        selectButton.isEnabled = false
        if multySelection != nil,
            let count = tableView.indexPathsForSelectedRows?.count,
            count > 0 {
            selectButton.isEnabled = true
        }
    }
        
    @IBAction private func selectButtonDidPress(_ sender: UIButton) {
        guard let pathes = tableView.indexPathsForSelectedRows else { return }
        
        let actions = pathes.flatMap { (path) -> Action? in
            return self.actions?[path.row]
        }
        
        multySelection?(actions)
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actions?.count ?? 0
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellIdentifier", for: indexPath)
        
        guard let action = actions?[indexPath.row] else { return cell }
        
        if let attributedTitle = action.attributedTitle {
            cell.textLabel?.attributedText = attributedTitle
            
        } else if let title = action.title {
            if actionAttributes != nil {
                cell.textLabel?.attributedText = NSAttributedString(string: title, attributes: actionAttributes)
            } else {
                cell.textLabel?.text = title
            }
        }
        
        cell.textLabel?.textAlignment = textAllignment
        
        return cell
    }

    override public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if (multySelection != nil) {
            updateSelectButton()
            return
        }
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (multySelection != nil) {
            updateSelectButton()
            return
        }
        
        guard let action = actions?[indexPath.row] else { return }
        action.completion?(action)
    }
    
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if multySelection != nil {
            return selectButton
        }
        return nil
    }
    
    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if multySelection != nil {
            return cellHeight
        }
        return 0
    }
    
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let selectedIndexes = tableView.indexPathsForSelectedRows {
            let selected = selectedIndexes.contains(where: { (selectedPath) -> Bool in
                return selectedPath == indexPath
            })
            cell.setSelected(selected, animated: false)
        }
    }
}

