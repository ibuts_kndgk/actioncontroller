//
//  ActionListViewController.swift
//  ActionContainerController
//
//  Created by Ihor Buts on 7/17/17.
//  Copyright © 2017 Ihor Buts. All rights reserved.
//

import UIKit
import ActionContainerController

open class ActionListViewController: ActionContainerViewController {
    
    private lazy var tableViewController = TableViewController()
    
    public var textAllignment: NSTextAlignment? {
        didSet {
            guard let textAllignment = textAllignment else { return }
            tableViewController.textAllignment = textAllignment
        }
    }
    
    public var actionAttributes: [String : Any]? {
        didSet {
            tableViewController.actionAttributes = actionAttributes
        }
    }
    
    public var actions: [Action]? {
        didSet {
            guard let actions = actions else { return }
            tableViewController.actions = actions
        }
    }
    
    public var cellHeight: CGFloat = 0.0 {
        didSet {
            tableViewController.cellHeight = cellHeight
            buttonHeight = cellHeight
        }
    }
    
    public var multySelection: (([Action]) -> Void)? {
        didSet {
            guard let multySelection = multySelection else { return }
            tableViewController.multySelection = multySelection
        }
    }
    
    private var maxContentSize: CGFloat {
        return UIScreen.main.bounds.height - titleLabel.frame.size.height - cellHeight - 40
    }
    
    override public init() {
        super.init()
        containerViewController = tableViewController
        cellHeight = 43.5
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        containerViewController = tableViewController
        cellHeight = 43.5
    }

    override open func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewController.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        if tableViewController.tableView.contentSize.height > maxContentSize {
            containerViewHeight = maxContentSize
        } else {
            containerViewHeight = tableViewController.tableView.contentSize.height
        }
        
    }
}

