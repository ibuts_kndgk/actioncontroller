//
//  ViewController.swift
//  TestProject
//
//  Created by Ihor Buts on 7/17/17.
//  Copyright © 2017 Ihor Buts. All rights reserved.
//

import UIKit
import ActionListController

class ViewController: UIViewController {

    @IBAction func didPress(_ sender: UIButton) {
        let controller = ActionListViewController()
        let action = Action()
        action.title = "Action"
        action.completion = { action in
//            self.dismiss(animated: true, completion: nil)
        }
        
        controller.multySelection = { (actions) in
            self.dismiss(animated: true, completion: nil)
        }
        
        controller.attributedTitle = NSAttributedString(string: "Title\nSubtitle")
        
        controller.cellHeight = 57
        controller.textAllignment = .center
        controller.actionAttributes = [NSForegroundColorAttributeName : UIColor(red: 19/255, green: 144/255, blue: 1, alpha: 1.0), NSFontAttributeName : UIFont.systemFont(ofSize: 21)]
        controller.actions = [action, action, action]//, action, action, action, action, action]
        
        self.present(controller, animated: true, completion: nil)
        
    }

}

